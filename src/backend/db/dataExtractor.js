fetch("https://www.nhm.ac.uk/discover/dino-directory/name/name-az-all.html")
  .then((res) => {
    return res.text()
  })
  .then((data) => {
    console.log("!")
    const parser = new DOMParser()
    const doc = parser.parseFromString(data, "text/html")
    let tags = [...doc.querySelectorAll(".dinosaurfilter--name-unhyphenated")]

    let dinosaurData = {
      data: [],
    }

    tags.forEach((tag) => {
      let dinoName = tag.innerHTML.replace(/\n|\t/g, "").toLowerCase()
      fetch(`https://www.nhm.ac.uk/discover/dino-directory/${dinoName}.html`)
        .then((res) => res.text())
        .then((data) => {
          const subDoc = parser.parseFromString(data, "text/html")
          const dinoContainer = subDoc.querySelector(".dinosaur--container")

          let dinosaur = {}

          //name
          try {
            dinosaur.name = ""
            dinosaur.name = dinoContainer.querySelector(
              ".dinosaur--name-unhyphenated"
            ).innerHTML
          } catch (err) {
          }
          try {
            dinosaur.img = ""
            dinosaur.img = dinoContainer.querySelector(".dinosaur--image").src
          } catch (err) {
          }
          try {
            dinosaur.type = ""
            dinosaur.type = dinoContainer.querySelector(
              ".dinosaur--description"
            ).children[1].children[0].innerHTML
          } catch (err) {
          }
          try {
            dinosaur.length = ""
            dinosaur.length = dinoContainer.querySelector(
              ".dinosaur--description"
            ).children[3].innerHTML
          } catch (err) {
          }

          try {
            dinosaur.diet = ""
            dinosaur.diet =
              dinoContainer.querySelector(
                ".dinosaur--info"
              ).children[1].children[0].innerHTML
          } catch (err) {
          }

          try {
            dinosaur.description = ""
            dinosaur.description =
              dinoContainer.children[3].children[0].innerHTML
                .replace(/\t|\n/g, "")
                .replace(/<[^>]+>/g, "")
          } catch (err) {
          }

          try {
            dinosaur.livedIn = ""
            let dinoInfo = [
              ...dinoContainer.querySelector(".dinosaur--info").children,
            ]
            dinoInfo.reduce((acc, next) => {
              if (next.innerHTML.includes("lived")) return true

              if (acc) {
                dinosaur.livedIn = next.innerHTML.replace(/<[^>]+>/g, "")
              }
              return false
            },false)

            // dinosaur.livedIn = dinoContainer
            //   .querySelector(".dinosaur--info")
            //   .children[3].innerHTML.replace(/<[^>]+>/g, "")
          } catch(err) {
          }

          try {
            dinosaur.taxonomy = ""
            dinosaur.taxonomy = dinoContainer
              .querySelector(".dinosaur--taxonomy")
              .children[1].innerHTML.split(", ")
          } catch (err) {
          }
          dinosaurData.data.push(dinosaur)
          console.log(JSON.stringify(dinosaurData))
        })
    })
  })
  .catch((err) => {
    console.log(err)
  })
