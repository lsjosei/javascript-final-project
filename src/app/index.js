import "./styles/styles.scss"

import { manageDisplay } from "./js/display"
import { getSortedData } from "./js/api"
import { filterSet } from "./js/addFunctions"
import { populateFilters, populateNameInput } from "./js/manageFilters"
import { toggleFav } from "./js/favFunctions"

window.onload = async () => {
  const main = document.querySelector(".main")
  const randomBtn = document.querySelector("#random-btn")
  const clearBtn = document.querySelector("#clear-btn")
  const favBtn = document.querySelector("#fav-btn")
  const dataUrl = "http://localhost:3000/data"

  const ageFilter = document.querySelector("#age-filter")
  const dietFilter = document.querySelector("#diet-filter")
  const typeFilter = document.querySelector("#type-filter")

  const nameFilter = document.querySelector("#dino-name")

  const btnLeft = document.querySelector(".hud-btn--left")
  const btnRight = document.querySelector(".hud-btn--right")

  //obtengo los datos originales
  const dinosaurData = await getSortedData(dataUrl)
  let currentFilter = { age: "", type: "", diet: "", name: "" }

  const clearFiltersValue = () => {
    ageFilter.value = ""
    typeFilter.value = ""
    dietFilter.value = ""
    nameFilter.value = ""
  }

  const resetFilter = () => {
    currentFilter = { age: "", type: "", diet: "", name: "" }
  }

  //currentData es una copia de los originales
  //pero que utilizo para almacenar versiones
  //filtradas del set de datos
  let currentData = [...dinosaurData]

  let isFavorite = false

  manageDisplay(
    filterSet(dinosaurData, currentFilter, isFavorite),
    main,
    toggleFav(currentData, dinosaurData),
    true,
    0,
    true
  )

  populateFilters(
    dinosaurData,
    ageFilter,
    dietFilter,
    typeFilter,
    currentFilter
  )

  //uso el scroll del raton para modificar
  //que tarjeta se esta mostrando
  main.addEventListener("wheel", (e) => {
    let mod = e.wheelDelta > 0 ? -1 : 1
    manageDisplay(
      filterSet(dinosaurData, currentFilter, isFavorite),
      main,
      toggleFav(currentData, dinosaurData),
      false,
      mod
    )
  })

  ageFilter.addEventListener("input", (e) => {
    //consigo los valores de filtro
    const filter = {
      age: e.target.value,
      type: typeFilter.value,
      diet: dietFilter.value,
      name: nameFilter.value,
    }

    currentFilter = filter
    manageDisplay(
      filterSet(dinosaurData, currentFilter, isFavorite),
      main,
      toggleFav(dinosaurData),
      true
    )

    populateFilters(
      dinosaurData,
      ageFilter,
      dietFilter,
      typeFilter,
      currentFilter
    )
  })

  dietFilter.addEventListener("input", (e) => {
    //consigo los valores de filtro
    const filter = {
      age: ageFilter.value,
      type: typeFilter.value,
      diet: e.target.value,
      name: nameFilter.value,
    }

    currentFilter = filter
    manageDisplay(
      filterSet(dinosaurData, currentFilter, isFavorite),
      main,
      toggleFav(dinosaurData),
      true
    )

    populateFilters(
      dinosaurData,
      ageFilter,
      dietFilter,
      typeFilter,
      currentFilter
    )
  })

  typeFilter.addEventListener("input", (e) => {
    //consigo los valores de filtro
    const filter = {
      age: ageFilter.value,
      type: e.target.value,
      diet: dietFilter.value,
      name: nameFilter.value,
    }

    currentFilter = filter
    manageDisplay(
      filterSet(dinosaurData, currentFilter, isFavorite),
      main,
      toggleFav(dinosaurData),
      true
    )

    populateFilters(
      dinosaurData,
      ageFilter,
      dietFilter,
      typeFilter,
      currentFilter
    )
  })

  nameFilter.addEventListener("input", (e) => {
    const filter = {
      age: ageFilter.value,
      type: typeFilter.value,
      diet: dietFilter.value,
      name: e.target.value,
    }
    currentFilter = filter
    manageDisplay(
      filterSet(dinosaurData, currentFilter, isFavorite),
      main,
      toggleFav(dinosaurData),
      true
    )

    const updateF = (name) => {
      return manageDisplay(
        filterSet(dinosaurData, { ...currentFilter, name: name }, isFavorite),
        main,
        toggleFav(dinosaurData),
        true
      )
    }
    populateNameInput(dinosaurData, currentFilter, isFavorite, updateF)
  })

  nameFilter.addEventListener("focusin", (e) => {
    const updateF = (name) => {
      return manageDisplay(
        filterSet(dinosaurData, { ...currentFilter, name: name }, isFavorite),
        main,
        toggleFav(dinosaurData),
        true
      )
    }
    populateNameInput(dinosaurData, currentFilter, isFavorite, updateF)
  })
  nameFilter.addEventListener("focusout", (e) => {
    setTimeout(() => {
      document
        .querySelector(".dino-name__options")
        .classList.add("not-displayed")
    }, 100)
  })

  clearBtn.addEventListener("click", (e) => {
    resetFilter()
    clearFiltersValue()
    isFavorite = false
    manageDisplay(
      filterSet(dinosaurData, currentFilter, isFavorite),
      main,
      toggleFav(dinosaurData),
      true
    )
  })
  randomBtn.addEventListener("click", (e) => {
    manageDisplay(
      filterSet(dinosaurData, currentFilter, isFavorite),
      main,
      toggleFav(dinosaurData),
      true,
      0,
      true
    )
  })
  favBtn.addEventListener("click", (e) => {
    resetFilter()
    clearFiltersValue()

    isFavorite = true
    manageDisplay(
      filterSet(dinosaurData, currentFilter, isFavorite),
      main,
      toggleFav(dinosaurData),
      true
    )
  })

  btnLeft.addEventListener("click", (e) => {
    manageDisplay(
      filterSet(dinosaurData, currentFilter, isFavorite),
      main,
      toggleFav(dinosaurData),
      false,
      -1
    )
  })
  btnRight.addEventListener("click", (e) => {
    manageDisplay(
      filterSet(dinosaurData, currentFilter, isFavorite),
      main,
      toggleFav(dinosaurData),
      false,
      1
    )
  })

  const showButton = document.querySelector(".input-section__show-btn")
  const inputSection = document.querySelector(".input-section")

  showButton.addEventListener("click", (e) => {
    inputSection.classList.toggle("hidden")
  })
}
