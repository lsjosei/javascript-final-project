import { displayFavs } from "./display"

const toggleFav = (dinosaurData) => {
  return (name) => {
    let dino = dinosaurData.find((dino) => dino.name === name)
    if ("isFav" in dino) {
      dino.isFav = !dino.isFav
    } else {
      dino.isFav = true
    }
    displayFavs(dinosaurData)
  }
}

const getFavorites = (dataset) => {
  return dataset.filter((dino) => {
    if ("isFav" in dino) {
      return dino.isFav
    }
    return false
  })
}

export { getFavorites, toggleFav }
