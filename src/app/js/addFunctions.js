import { getFavorites } from "./favFunctions"

const filterByProps = (dataset, filter) => {
  return dataset.filter((dino) => {
    return [...Object.keys(filter)].every((ele) => {
      if (ele === "name") return true
      return filter[ele] === "" || dino[ele].includes(filter[ele])
    })
  })
}

const filterByName = (dataset, name) => {
  return dataset.filter(
    (ele) => name === "" || ele.name.toLowerCase().includes(name.toLowerCase())
  )
}

const filterSet = (
  dataset,
  filter = { age: "", diet: "", type: "", name: "" },
  favs = false
) => {
  if (favs) return getFavorites(dataset)
  return filterByName(filterByProps(dataset, filter), filter.name)
}

const sortByAge = (a, b) => {
  const eraOrder = {
    Cretaceous: 0,
    Jurassic: 1,
    Triassic: 2,
    Early: 0.2,
    Mid: 0.5,
    Late: 0.8,
  }
  let aValue = a.split(" ").reduce((acc, value) => acc + eraOrder[value], 0)
  let bValue = b.split(" ").reduce((acc, value) => acc + eraOrder[value], 0)
  return aValue - bValue
}

const sortByType = (a, b) => {
  let termA = a.split(" ").slice(-1)
  let termB = b.split(" ").slice(-1)
  return termA >= termB ? 1 : -1
}

const extractValue = (ele, type) => {
  if (type == "age") {
    return ele.age.split(",")[0]
  }
  return ele[type]
}

export { filterSet, extractValue, sortByAge, sortByType }
