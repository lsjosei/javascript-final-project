const getSortedData = async (url) => {
  //funcion asincrona para recuperar datos de una url
  const res = await fetch(url)
  let data = await res.json()
  data = await data.sort((a, b) => a.name - b.name)
  console.log(data)
  return data
}

export { getSortedData }
