import { filterSet } from "./addFunctions"
import { toggleFav } from "./favFunctions"

const getElementWithClass = (container, className) => {
  //recupero el objeto (child) y su indice dentro de un container
  //cuando lleve la clase selected
  let selectedDino = [...container.children].find((ele) =>
    ele.classList.contains(className)
  )
  return selectedDino
}

const generateFavButton = (container, dino, saveFunction) => {
  let favBtn = document.createElement("div")
  favBtn.classList.add("btn-fav")
  container.appendChild(favBtn)
  favBtn.classList.remove("btn-fav__fav")
  if (dino.isFav) {
    favBtn.classList.add("btn-fav__fav")
  }

  favBtn.addEventListener("click", (e) => {
    saveFunction(dino.name)
    favBtn.classList.remove("btn-fav__fav")
    if (dino.isFav) {
      favBtn.classList.add("btn-fav__fav")
    }
  })
}

const displayCard = (
  dino,
  container,
  index,
  counter,
  window,
  saveFunction,
  selected = false
) => {
  //articulo es la tarjeta
  let art = document.createElement("article")
  container.appendChild(art)
  art.classList.add("card")

  if (selected) art.classList.add("selected")

  //este index me permite saber su posicion
  //dentro del dataset
  art.dataset.index = index

  //hago un desplazamiento vertical + horizontal
  //para mostrar las tarjetas solapadas
  art.style.top = `calc(80% - ${(window / 2 - counter) * 24}px)`
  let mod = parseInt(window / 2) != window / 2 ? 1 : 0 //esto ajusta el central en funcion de si el length del set es par o impar
  art.style.left = `calc(50% - ${
    (parseInt(window / 2) - counter + mod) * 24
  }px)`

  //aqui genero la etiqueta trapezoidal de
  //encima de la tarjeta
  let tag = document.createElement("div")
  art.appendChild(tag)

  tag.classList.add("card__tag")

  tag.innerHTML = dino.name

  //aqui genero el interior de la tarjeta
  //en algunos casos no tengo descripcion, asi que
  //lo compruebo y lo dejo en blanco si no hay
  if (selected) {
    let desc =
      dino.description !== ""
        ? `<span><b>Description</b>: ${dino.description}</span>`
        : ""

    let dinoInfo = `<div class="card__info">
  <div class="card__img-container">
  <img class="card__img" src="${dino.img}">
  </div>
  <div class="card__data">
  <span><b>Name</b>: ${dino.name}</span>
  <span><b>Age</b>: ${dino.age}</span>
  <span><b>Type</b>: ${dino.type}</span>
  <span><b>Diet</b>: ${dino.diet}</span>
  <span><b>Length</b>: ${dino.length}</span>
  ${desc}
  </div>
  </div>`

    art.innerHTML += dinoInfo

    generateFavButton(art, dino, saveFunction)
  }
}

const readjustThreshold = (thr, selected, datasetLength) => {
  if (selected <= thr) return thr + thr - selected
  if (selected >= datasetLength - thr)
    return thr + thr - (datasetLength - selected) + 1
  return thr
}

const display = (dataset, container, selected, saveFunction) => {
  container.innerHTML = ""
  let counter = 0
  //thr es la mitad del ancho de la ventana
  //de tarjetas que enseño, p.e.:
  //10 significa que muestro 10 tarjetas
  //antes y despues de la seleccionada, 21
  //en total

  //ajusto thr para que cerca de los bordes,
  //cuando quedan menos tarjetas en uno de los lados
  //que el thr, se muestren más por el otro, y
  //así siempre mostrar las mismas
  const thr = readjustThreshold(2, selected, dataset.length)

  //displayLength me permite distribuir el grupo
  //de tarjetas en el centro de la pantalla
  const displayLength = dataset.filter(
    (_, index) => Math.abs(index - selected) <= thr
  ).length

  dataset.forEach((dino, index) => {
    if (Math.abs(index - selected) <= thr) {
      displayCard(
        dino,
        container,
        index,
        counter,
        displayLength / 2,
        saveFunction,
        index === selected
      )
      counter++
    }
  })
}

const getIndexOfDino = (dataset, currentDinosaur) => {
  for (let i = 0; i < dataset.length; i++) {
    if (dataset[i].name === currentDinosaur) {
      return i
    }
  }
}

const manageDisplay = (
  dataset,
  container,
  saveFunction,
  overrideSelection = false,
  transition = 0,
  random = false
) => {
  let selected = random ? Math.floor(Math.random() * dataset.length) : 0
  let selectedDOM = getElementWithClass(container, "selected")

  if (overrideSelection)
    return display(dataset, container, selected, saveFunction)
  if (dataset.length < 1 || !selectedDOM)
    return display(dataset, container, selected, saveFunction)

  const currentDinosaur = selectedDOM.querySelector(".card__tag").innerHTML

  const isPresent =
    dataset.filter((ele) => ele.name == currentDinosaur).length > 0

  //si se mantiene recojo su indice y lo utilizo
  //para volver a mostrarlo en foco
  //si no esta presente reseteo el seleccionado
  //al primero del set
  selected = isPresent ? getIndexOfDino(dataset, currentDinosaur) : selected

  //gestiono que no se puedan superar los limites del set
  const isDown = selected > 0 && transition === -1
  const isUp = selected < dataset.length - 1 && transition === 1
  if (isUp || isDown) {
    selectedDOM.classList.add("discarted")
    selectedDOM.addEventListener("animationend", (e) => {
      display(dataset, container, selected + transition, saveFunction)
    })
  }
}

const displayFavs = (dinosaurData) => {
  const container = document.querySelector(".fav-list")
  container.innerHTML = "<span class='fav-list__title'>Favorite List</span>"
  dinosaurData
    .filter((dino) => dino.isFav)
    .forEach((dino) => {
      let pEle = document.createElement("p")
      pEle.innerHTML = dino.name
      container.appendChild(pEle)
      pEle.addEventListener("click", (e) => {
        manageDisplay(
          filterSet(dinosaurData, {
            age: "",
            type: "",
            diet: "",
            name: dino.name,
          }, false),
          document.querySelector(".main"),
          toggleFav(dinosaurData),
          true
        )
      })
    })
}

export { manageDisplay, displayFavs }
