import { filterSet, sortByAge, sortByType, extractValue } from "./addFunctions"

const generateOptions = (filter, options, currentValue) => {
  filter.innerHTML = "<option value=''>-</option>"
  options.forEach((option) => {
    filter.innerHTML += `
    <option value="${option}">${option}</option>`
  })

  //como he vuelto a popular el filtro, el valor por defecto
  //es el primero, "-" siempre por lo tanto
  //así que le asigno el valor original que tenia antes
  filter.value = currentValue
}

const populateIndividualFilter = (
  ageFilter,
  completeSet,
  currentValue,
  filter,
  type,
  sortF
) => {
  if (ageFilter) {
    const filteredSet = filterSet(completeSet, filter)
    const agesAll = [
      ...new Set(filteredSet.map((dino) => extractValue(dino, type))),
    ]
      .filter((ele) => ele)
      //ordeno las eras para que aparezcan de manera cronologica
      .sort(sortF)

    generateOptions(ageFilter, agesAll, currentValue)
  }
}

const populateFilters = (
  completeSet,
  ageFilter,
  dietFilter,
  typeFilter,
  filter = { age: "", diet: "", type: "", name: "" }
) => {
  //poblar los filters
  populateIndividualFilter(
    ageFilter,
    completeSet,
    filter.age,
    { ...filter, ...{ age: "" } },
    "age",
    sortByAge
  )
  populateIndividualFilter(
    typeFilter,
    completeSet,
    filter.type,
    { ...filter, ...{ type: "" } },
    "type",
    sortByType
  )
  populateIndividualFilter(
    dietFilter,
    completeSet,
    filter.diet,
    { ...filter, ...{ diet: "" } },
    "diet"
  )
}

const populateNameInput = (dataset, currentFilter, isFavorite, updateDisplayFunction) => {
  const filter = document.querySelector(".dino-name__options")
  filter.innerHTML = ""
  filter
    .classList.remove("not-displayed")
  filterSet(dataset, currentFilter, isFavorite)
    .forEach((dino) => {
      let pEle = document.createElement("p")
      pEle.innerHTML = dino.name
      filter.appendChild(pEle)
      pEle.addEventListener("click", (e) => {
        updateDisplayFunction(dino.name)
      })
    })
}

export { populateFilters, populateNameInput }
